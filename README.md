## Installation

```sh
npm install
```

## Running

1st terminal:
```sh
❯ node index.js
```

2nd terminal:
```sh
❯ curl -v http://localhost:3002/foobar/pepa
```

This creates connects to database: `./something-else.sqlite` and creates new record in table `bike_models` with `{name: 'pepa'}`.

```sh
❯ curl -v http://localhost:3002/cool-db/josef
```

This creates connects to database: `./cool-db.sqlite` and creates new record in table `bike_models` with `{name: 'josef'}`.
