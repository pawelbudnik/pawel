const express = require('express')
const app = express()
const port = 3002
const router = express.Router()
const dbSwitch = require('./db_switch');
const bikeModel = require('./bike_model');

app.use('/', dbSwitch);

app.use('/*/:whatever', async (req, res, next) => {
  console.log(`second path parameter is "${req.params.whatever}" and we have db connection to "${req.db}"`)

  const BikesModel = bikeModel(req.db);

  await BikesModel.sync();

  const myBike = await BikesModel.create({name: req.params.whatever});
})

app.listen(port, () => {
})